FROM opensuse

ADD ./distr /distr

RUN zypper --non-interactive --no-gpg-checks update
RUN zypper --non-interactive --no-gpg-checks install wget unzip git
RUN rpm -i /distr/jdk-8u162-linux-x64.rpm
RUN wget http://apache-mirror.rbc.ru/pub/apache/maven/maven-3/3.5.3/binaries/apache-maven-3.5.3-bin.zip -O /distr/maven.zip
RUN unzip /distr/maven.zip -d /opt/
RUN mv /opt/apache-maven-3.5.3 /opt/mvn

ENV JAVA_HOME=usr/java/jdk1.8.0_162
ENV PATH=$PATH:/opt/mvn/bin

#CMD ["/bin/bash"]
